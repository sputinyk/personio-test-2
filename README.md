# Personio application test

## Running test

### Installation

- Install dependencies:
  - `yarn`
- In the root folder, create a `.env` file using the `env.example` file as example (you can run `cp .env.example .env`). This is because we can run the application in different environments, and is not a good idea versioning environment specific values.

### Running in development mode

- After install dependencies, start the application:
  - Run `yarn start`

### Running in production mode

- After installing dependencies, create a production build:
  - Run `yarn build`
  - A build project will be generated inside `dist` folder, execute this content using some web server (nginx, apache, node). I'm using node [http-server](https://www.npmjs.com/package/http-server).

## Important questions

- Why don't using [react-router](https://reacttraining.com/react-router/)?
  - React router is awesome for routing a multiple-pages app, but has no good support for query strings (add, get, parse, etc.). So, I'm using another small solution for handle this part. When the application will have more pages, the `src/components/App.jsx` is a good place to declare the application routes;
- Why don't using friendly URLs?
  - Friendly URLs are good for routing system, but for a search form with a shareable URL, build the parameters with query string is better;
- Why the application use CSS in JS approach to applying styles?
  - Please read [this article](https://objectpartners.com/2017/11/03/css-in-js-benefits-drawbacks-and-tooling/);
- Why the filters and sorting values are present in URL and in `src/pages/Applications.jsx` component, instead of keep in a single place?
  - Keep the values only URL doesn't trigger react re-render components when URL change, is too hard to keep this synced, forcing use of [forceUpdate()](https://reactjs.org/docs/react-component.html#forceupdate) method usage, and this should be avoided. I'm updating URL just for share reasons, and when the page open, the application fill form and filters with the initial URL parameters values. After that, all the filters and sorting values are maintained by the component state;
- Why don't using helpers libraries (lodash, underscore, moment.js, etc.)?
  - For now, I didn't feel the necessity to load any helper libraries. Is not a good idea load [64kb just for format a string date](https://bundlephobia.com/result?p=moment@2.22.2). When this will be necessary, the application can receive these helpers.

## Project structure and responsabilities (inside src folder):

- components
  - Datagrid
    - index.jsx
      - A Datagrid component, built with Table component and enhanced with `sortable` feature, using the `Sorter` component. In the future, more features can be added, like pagination.
    - SortButton.jsx
      - Just a button labeled with arrows.
  - Alert.jsx
    - A simple component used to display user messages. For this challenge, only error option is available but is possible to add other states (success, info, warning, etc.).
  - App.jsx
    - Application wrapper component. A good place for put routes, use a redux store, etc.
  - Card.jsx
    - A card component, just to wrapper components into boxes with optional titles. If necessary, can be improved with a footer, controls, collapsible features, etc.
  - Container.jsx
    - A generic component used to compose layouts.
  - Form.jsx
    - A form component with inputs (can be exported separately). For now, only the form, text input, select and radio group are available, but new fields can be added (when more code will be added in this file, maybe a good idea separate in different files, like Datagrid component). The form and the components are connected using the [context API](https://reactjs.org/docs/context.html), and all the logic is stored inside a `Form` component (it works similar to [Redux Form](https://redux-form.com)). This way, the fields are more simple, and the form operations can be made in Form component (watch value, changes, get the inputs values, submit the form, apply validation, fill the form, etc.), and even with big forms, the code keeps clean. Currently, only the necessary methods for this test are implemented but is easly to improve this component (with validation for example).
  - Icons.jsx
    - A collection of react components who render SVG icons
  - Loader.jsx
    - A loader component, used to represent a waiting moment while some operation occurs.
  - Page.jsx
    - A generic page wrapper component. A good place to put the header, menu, footer, etc.
  - Sorter.jsx
    - A generic sorter component utility using [render prop](https://reactjs.org/docs/render-props.html) approach, used to sort any kind of collection (table, list, etc.). Currently, it's used by `Datagrid` component
  - Table.jsx
    - A table component with nested useful table components (tbody, thead, row, cell, etc.). I used this approach because normally all these components are used together. This is the same strategy used by other libraries, like [Semantic UI](https://react.semantic-ui.com/collections/table/)
  - Title.jsx
    - Just a simple title component, with an optional icon
- constants
  - This folder contains the constants files. For now, just theme related constants are there.
- pages
  - Applications.jsx
    - Applications page component is a container component responsible for fetch data and sync URL query string parameters with filters (sending filters to query string and watching history changes to apply state component changes).
- utils
  - date.js
    - Date helpers (until now, just a function to calculate age, and format a date string)
  - querystring.js
    - This file export a class used to get and set query string values
  - request.js
    - This is a wrapper for [axios](https://github.com/axios/axios) library, set general parameters (like base URL, authentication headers, etc.). And I'm using for handle API error responses (even an error request, the API return a 200 response code, and I handling this to avoid ugly checks in every application request implementation).
- index.jsx
  - Application bootstrap file. Just import and load the main component and mount in DOM
