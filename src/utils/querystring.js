import qs from 'querystringify';

class QueryString {
  static set(params) {
    const querystring = Object.keys(params).length ? qs.stringify(params, true) : '/';
    window.history.pushState(null, null, querystring);
  }

  static all() {
    return qs.parse(window.location.search);
  }
}

export default QueryString;
