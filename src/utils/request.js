import axios from 'axios';
import { API_URL } from '@env';

const request = axios.create({
  baseURL: API_URL,
});

request.interceptors.response.use(
  response => (response.data.error ? Promise.reject(response.data.error) : response),
  error => Promise.reject(error),
);

export function getCancelTokenSource() {
  const cancelToken = axios.CancelToken;
  return cancelToken.source();
}

export default request;
