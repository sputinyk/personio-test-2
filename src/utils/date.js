export function ageByDate(dt) {
  if (Number.isNaN(Date.parse(dt))) {
    return '';
  }

  const today = new Date();
  const birthDate = new Date(dt);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }

  return age;
}

export function formatDate(dt) {
  if (!dt) {
    return '';
  }

  return dt
    .split('-')
    .reverse()
    .join('/');
}
