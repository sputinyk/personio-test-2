export const COLOR_PRIMARY = '#daf2f8';
export const COLOR_NEUTRAL_1 = '#fff';
export const COLOR_NEUTRAL_2 = '#f1f1f1';
export const COLOR_NEUTRAL_3 = '#f2f2f2';
export const COLOR_ACCENT = '#ffff00';

export const COLOR_TEXT = '#252525';

export const COLOR_ERROR_PRIMARY = '#d9534f';
export const COLOR_ERROR_SECONDARY = '#fdf7f7';

export const FONT_FAMILY = 'Montserrat, sans-serif';
export const TRANSITION = 'all 0.2s ease-in-out';
export const BOX_SHADOW = '0 3px 7px -1px rgba(0, 0, 0, 0.1)';
