/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import { Component, createContext } from 'react';
import PropTypes from 'prop-types';

import { COLOR_TEXT, FONT_FAMILY, TRANSITION } from '../constants/theme';

const FormContext = createContext({ onChange: () => {}, values: {} });

const sharedPropTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
const itemsListPropTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string.isRequired, value: PropTypes.string.isRequired }),
  ).isRequired,
};

const commomInputStyle = css`
  border: 1px solid #ccc;
  outline: none;
  border-radius: 2px;
  height: 30px;
  padding: 0 5px;
  transition: ${TRANSITION};
  &:focus {
    border: 1px solid ${COLOR_TEXT};
  }
`;

const formItemWrapperStyle = css`
  margin: 5px;
`;

const InputLabel = ({ text }) => (
  <span
    css={css`
      font-family: ${FONT_FAMILY};
      font-size: 14px;
      padding: 0 5px;
    `}
  >
    {text}
  </span>
);
InputLabel.propTypes = {
  text: PropTypes.string.isRequired,
};

class Form extends Component {
  state = {
    values: {},
  };

  handleChangeInput = (name, value) => {
    const { onChange } = this.props;
    this.setState(state => {
      const { values } = Object.assign({}, state);
      values[name] = value;
      onChange(values, name);
      return { values };
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { values } = this.state;
    const { onSubmit } = this.props;
    onSubmit(values);
  };

  fill(values) {
    this.setState({ values });
  }

  render() {
    const { children } = this.props;
    const { values } = this.state;
    const context = {
      onChange: this.handleChangeInput,
      values,
    };
    return (
      <FormContext.Provider value={context}>
        <form onSubmit={this.handleSubmit} autoComplete="off">
          {children}
        </form>
      </FormContext.Provider>
    );
  }
}
Form.defaultProps = {
  onSubmit: () => {},
  onChange: () => {},
};
Form.propTypes = {
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  children: PropTypes.node.isRequired,
};

const Input = ({ label, name }) => (
  <FormContext.Consumer>
    {({ onChange, values }) => (
      <label css={formItemWrapperStyle}>
        <InputLabel text={label} />
        <input
          type="text"
          name={name}
          css={commomInputStyle}
          value={values[name] || ''}
          onChange={({ target: { value } }) => onChange(name, value)}
        />
      </label>
    )}
  </FormContext.Consumer>
);
Input.propTypes = sharedPropTypes;

const Select = ({ label, name, items, emptyOption }) => (
  <FormContext.Consumer>
    {({ onChange, values }) => (
      <label css={formItemWrapperStyle}>
        <InputLabel text={label} />
        <select
          name={name}
          css={commomInputStyle}
          onChange={({ target: { value } }) => onChange(name, value)}
          value={values[name] || ''}
        >
          {emptyOption !== null && <option value="">{emptyOption}</option>}
          {items.map(option => (
            <option key={option.value} disabled={!!option.disabled} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </label>
    )}
  </FormContext.Consumer>
);
Select.defaultProps = {
  emptyOption: null,
};
Select.propTypes = {
  ...sharedPropTypes,
  ...itemsListPropTypes,
  emptyOption: PropTypes.string,
};

const RadioGroup = ({ label, name, items }) => (
  <FormContext.Consumer>
    {({ onChange, values }) => (
      <fieldset
        css={css`
          ${formItemWrapperStyle}
          padding: 1px;
          border: none;
        `}
      >
        <legend
          css={css`
            font-family: ${FONT_FAMILY};
            font-size: 14px;
          `}
        >
          {label}
        </legend>
        {items.map(option => (
          <label
            key={option.value}
            css={css`
              margin-top: 0px;
              font-family: ${FONT_FAMILY};
              font-size: 12px;
              padding: 0 5px;
            `}
          >
            <input
              type="radio"
              name={name}
              value={option.value}
              checked={values[name] === option.value}
              onChange={() => onChange(name, option.value)}
            />
            {option.label}
          </label>
        ))}
      </fieldset>
    )}
  </FormContext.Consumer>
);
RadioGroup.propTypes = {
  ...sharedPropTypes,
  ...itemsListPropTypes,
};

export default Form;
export { Input, Select, RadioGroup };
