const Sorter = ({ data, sortBy, sortType, children }) => {
  const sorted = [...data];
  if (sortBy) {
    const direction = sortType || 'asc';
    sorted.sort((a, b) => (a[sortBy] > b[sortBy] ? 1 : -1));
    if (direction === 'desc') {
      sorted.reverse();
    }
  }
  return children(sorted);
};

export default Sorter;
