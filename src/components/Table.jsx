/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import {
  FONT_FAMILY,
  COLOR_ACCENT,
  COLOR_TEXT,
  COLOR_NEUTRAL_1,
  COLOR_NEUTRAL_3,
  COLOR_PRIMARY,
  TRANSITION,
} from '../constants/theme';

const tableItemPropTypes = {
  children: PropTypes.node.isRequired,
};

const Table = ({ children }) => (
  <table
    css={css`
      border-collapse: collapse;
      font-size: 14px;
      font-family: ${FONT_FAMILY};
      color: ${COLOR_TEXT};
      border: 1px solid #ccc;
      background: ${COLOR_NEUTRAL_1};
      width: 100%;
    `}
  >
    {children}
  </table>
);
Table.propTypes = tableItemPropTypes;

Table.Header = ({ children }) => (
  <thead
    css={css`
      background: ${COLOR_ACCENT};
      border-bottom: 1px solid #ccc;
      th {
        text-align: center;
      }
    `}
  >
    {children}
  </thead>
);
Table.Header.propTypes = tableItemPropTypes;

Table.Body = ({ children }) => (
  <tbody
    css={css`
      tr {
        cursor: default;
        &:nth-of-type(even) {
          background-color: ${COLOR_NEUTRAL_3};
        }
        transition: ${TRANSITION};
        &:hover {
          background-color: ${COLOR_PRIMARY};
        }
      }
    `}
  >
    {children}
  </tbody>
);
Table.Body.propTypes = tableItemPropTypes;

Table.Row = ({ children }) => <tr>{children}</tr>;
Table.Row.propTypes = tableItemPropTypes;

Table.HeaderCell = ({ children }) => (
  <th
    css={css`
      border: 1px solid #ccc;
      padding: 5px;
    `}
  >
    {children}
  </th>
);
Table.HeaderCell.propTypes = tableItemPropTypes;

Table.Cell = ({ children }) => (
  <td
    css={css`
      padding: 5px;
    `}
  >
    {children}
  </td>
);
Table.Cell.propTypes = tableItemPropTypes;

export default Table;
