/** @jsx jsx */
import { jsx, css, keyframes } from '@emotion/core';
import PropTypes from 'prop-types';

import Container from './Container';
import { COLOR_TEXT, FONT_FAMILY, COLOR_PRIMARY, COLOR_NEUTRAL_3 } from '../constants/theme';

const rotateKeyframe = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Loader = ({ text }) => (
  <Container>
    <div
      css={css`
        margin: 10px;
        width: 25px;
        height: 25px;
        border: 3px solid ${COLOR_NEUTRAL_3};
        border-radius: 50%;
        border-top-color: ${COLOR_PRIMARY};
        animation: ${rotateKeyframe} 1s ease-in-out infinite;
      `}
    />
    <span
      css={css`
        padding: 5px 0;
        font-size: 0.8em;
        font-family: ${FONT_FAMILY};
        color: ${COLOR_TEXT};
      `}
    >
      {text}
    </span>
  </Container>
);
Loader.defaultProps = {
  text: 'Loading',
};
Loader.propTypes = {
  text: PropTypes.string,
};

export default Loader;
