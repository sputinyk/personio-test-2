/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import { COLOR_TEXT, FONT_FAMILY } from '../../constants/theme';

import Table from '../Table';
import Loader from '../Loader';
import Card from '../Card';
import Sorter from '../Sorter';
import SortButton from './SortButton';

function getCounterText(qtd) {
  if (qtd > 1) {
    return `${qtd} records found`;
  }

  if (qtd === 1) {
    return '1 record found';
  }

  return 'No records found';
}

const Datagrid = ({
  title,
  toolbar,
  headers,
  data,
  rowKey,
  rowColumns,
  formatter,
  onSort,
  sortBy,
  sortType,
  isLoading,
}) => (
  <Card title={title}>
    {toolbar}
    <p
      css={css`
        color: ${COLOR_TEXT};
        font-family: ${FONT_FAMILY};
        margin: 10px 0 5px 0;
        font-size: 0.8em;
      `}
    >
      {getCounterText(data.length)}
    </p>
    <Table>
      <Table.Header>
        <Table.Row>
          {headers.map((header, index) => {
            const value = typeof header === 'string' ? header : header.label;
            const sortable = !!header.sortable;
            const direction = sortBy === rowColumns[index] ? sortType : null;
            const nextSort = sortType === 'asc' && direction ? 'desc' : 'asc';
            return (
              <Table.HeaderCell key={value}>
                {sortable ? (
                  <SortButton
                    label={value}
                    direction={direction}
                    onClick={() => onSort(rowColumns[index], nextSort)}
                  />
                ) : (
                  value
                )}
              </Table.HeaderCell>
            );
          })}
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Sorter data={data} sortBy={sortBy} sortType={sortType}>
          {sortedData =>
            sortedData.map(item => (
              <Table.Row key={item[rowKey]}>
                {rowColumns.map(key => {
                  const value = formatter[key] ? formatter[key](item[key]) : item[key];
                  return <Table.Cell key={key}>{value}</Table.Cell>;
                })}
              </Table.Row>
            ))
          }
        </Sorter>
      </Table.Body>
    </Table>
    {isLoading && <Loader text="Loading items" />}
  </Card>
);
Datagrid.defaultProps = {
  title: null,
  toolbar: null,
  formatter: {},
  rowKey: 'id',
  sortBy: null,
  sortType: null,
  isLoading: false,
  onSort: () => {},
};
Datagrid.propTypes = {
  title: PropTypes.string,
  sortBy: PropTypes.string,
  sortType: PropTypes.string,
  isLoading: PropTypes.bool,
  onSort: PropTypes.func,
  toolbar: PropTypes.node,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  rowColumns: PropTypes.arrayOf(PropTypes.string).isRequired,
  headers: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        sortable: PropTypes.bool.isRequired,
      }),
    ]),
  ).isRequired,
  formatter: PropTypes.shape({}),
  rowKey: PropTypes.string,
};

export default Datagrid;
