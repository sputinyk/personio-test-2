/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import { TRANSITION } from '../../constants/theme';
import { ArrowUpDown, ArrowDown, ArrowUp } from '../Icons';

const SortButton = ({ label, direction, onClick }) => {
  let icon = <ArrowUpDown />;
  let opacity = 0.2;
  if (direction) {
    icon = direction === 'asc' ? <ArrowUp /> : <ArrowDown />;
    opacity = 1;
  }
  return (
    <button
      type="button"
      css={css`
        width: 100%;
        padding: 0;
        background: none;
        border: none;
        display: block;
        height: 25px;
        font-size: 14px;
        font-weight: bold;
        display: flex;
        svg {
          opacity: ${opacity};
          transition: ${TRANSITION};
        }
        &:hover svg {
          opacity: 1;
        }
      `}
      onClick={onClick}
    >
      {icon}
      <span
        css={css`
          line-height: 25px;
        `}
      >
        {label}
      </span>
    </button>
  );
};
SortButton.defaultProps = {
  direction: null,
};
SortButton.propTypes = {
  label: PropTypes.string.isRequired,
  direction: PropTypes.oneOf(['asc', 'desc']),
  onClick: PropTypes.func.isRequired,
};

export default SortButton;
