/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import { COLOR_NEUTRAL_1, COLOR_PRIMARY, FONT_FAMILY, BOX_SHADOW } from '../constants/theme';

const Card = ({ children, title }) => (
  <section
    css={css`
      margin: 5px auto;
      border-radius: 5px;
      box-shadow: ${BOX_SHADOW};
      margin-bottom: 1.6%;
      background: ${COLOR_NEUTRAL_1};
      width: 100%;
    `}
  >
    {title && (
      <h3
        css={css`
          font-family: ${FONT_FAMILY};
          font-size: 20px;
          margin-top: 0;
          margin-bottom: 5px;
          padding: 5px;
          background: ${COLOR_PRIMARY};
        `}
      >
        {title}
      </h3>
    )}
    <div
      css={css`
        padding: 5px;
      `}
    >
      {children}
    </div>
  </section>
);
Card.defaultProps = {
  title: null,
};
Card.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default Card;
