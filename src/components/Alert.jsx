/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import {
  COLOR_ERROR_PRIMARY,
  COLOR_ERROR_SECONDARY,
  FONT_FAMILY,
  BOX_SHADOW,
} from '../constants/theme';

const textColors = {
  error: COLOR_ERROR_PRIMARY,
};
const bgColors = {
  error: COLOR_ERROR_SECONDARY,
};

const Alert = ({ title, text, type }) => (
  <div
    css={css`
      margin: 10px 0;
      padding: 10px;
      border-left: 3px solid;
      border-color: ${textColors[type]};
      background-color: ${bgColors[type]};
      font-family: ${FONT_FAMILY};
      box-shadow: ${BOX_SHADOW};
    `}
  >
    {title && (
      <h4
        css={css`
          margin: 5px 0;
          color: ${textColors[type]};
        `}
      >
        {title}
      </h4>
    )}
    <p
      css={css`
        margin: 0;
        font-size: 0.85em;
      `}
    >
      {text}
    </p>
  </div>
);
Alert.defaultProps = {
  title: null,
  type: 'error',
};
Alert.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['error']),
};

export default Alert;
