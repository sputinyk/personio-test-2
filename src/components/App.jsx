import React, { Fragment } from 'react';
import { Global, css } from '@emotion/core';

import { COLOR_NEUTRAL_2 } from '../constants/theme';
import Applications from '../pages/Applications';

const App = () => (
  <Fragment>
    <Global
      styles={css`
        body {
          margin: 0;
          background: ${COLOR_NEUTRAL_2};
        }
      `}
    />
    <Applications />
  </Fragment>
);

export default App;
