/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

const IconBase = ({ path, viewBox }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox={viewBox}
    css={css`
      width: 25px;
      height: 25px;
    `}
  >
    <path d={path} />
  </svg>
);
IconBase.defaultProps = {
  viewBox: '0 0 50 50',
};
IconBase.propTypes = {
  viewBox: PropTypes.string,
  path: PropTypes.string.isRequired,
};

const ArrowUpDown = () => (
  <IconBase path="M 25 3 L 7 22 L 43 22 L 25 3 z M 7 26 L 25 45 L 43 26 L 7 26 z" />
);
const ArrowDown = () => <IconBase path="M 4 17 L 25 39 L 46 17 L 4 17 z" />;
const ArrowUp = () => <IconBase path="M 25 13 L 4 35 L 46 35 L 25 13 z" />;
const User = () => (
  <IconBase
    viewBox="0 0 24 24"
    path="M 12 0 C 7.582 0 4 3.582 4 8 C 4 12.418 7.582 16 12 16 C 16.418 16 20 12.418 20 8 C 20 3.582 16.418 0 12 0 z M 6.65625 10.65625 L 17.34375 10.65625 C 16.35875 12.62425 14.351 14 12 14 C 9.649 14 7.64125 12.62425 6.65625 10.65625 z M 7.375 17.375 C 4.399 18.157 2.069 20.015 1 24 L 23 24 C 21.931 20.014 19.603 18.156 16.625 17.375 C 15.247 18.113 13.671 18.53125 12 18.53125 C 10.33 18.53125 8.753 18.115 7.375 17.375 z"
  />
);

export { ArrowUpDown, ArrowDown, ArrowUp, User };
