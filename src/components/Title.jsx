/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

import { FONT_FAMILY } from '../constants/theme';

const Title = ({ text, icon }) => (
  <h1
    css={css`
      font-family: ${FONT_FAMILY};
      font-size: 26px;
      margin: 10px;
      svg {
        margin-right: 5px;
      }
    `}
  >
    {icon}
    {text}
  </h1>
);
Title.defaultProps = {
  icon: null,
};
Title.propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.element,
};

export default Title;
