/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

const Container = ({ children, direction, align }) => (
  <section
    css={css`
      display: flex;
      flex-direction: ${direction};
      align-items: ${align};
    `}
  >
    {children}
  </section>
);
Container.defaultProps = {
  direction: 'column',
  align: 'center',
};
Container.propTypes = {
  children: PropTypes.node.isRequired,
  direction: PropTypes.oneOf(['column', 'row']),
  align: PropTypes.oneOf(['center', 'flex-start', 'flex-end']),
};

export default Container;
