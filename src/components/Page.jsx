/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import PropTypes from 'prop-types';

const Page = ({ children }) => (
  <div
    css={css`
      max-width: 1280px;
      margin: 0 auto;
    `}
  >
    {children}
  </div>
);
Page.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Page;
