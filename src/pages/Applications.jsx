import React from 'react';

import Page from '../components/Page';
import Title from '../components/Title';
import Datagrid from '../components/Datagrid';
import Container from '../components/Container';
import Form, { Input, Select, RadioGroup } from '../components/Form';
import { User as UserIcon } from '../components/Icons';
import { ageByDate, formatDate } from '../utils/date';
import QueryString from '../utils/querystring';
import request, { getCancelTokenSource } from '../utils/request';

const Alert = React.lazy(() => import('../components/Alert'));

class Applications extends React.Component {
  constructor(...args) {
    super(...args);

    const { sortBy, sortType, status, position, name } = QueryString.all();
    this.state = {
      isLoading: true,
      errorMessage: null,
      data: [],
      sortBy,
      sortType,
      filters: {
        name: name || '',
        position: position || '',
        status: status || '',
      },
    };

    this.requestApplicationsTokenSource = getCancelTokenSource();

    this.formRef = React.createRef();
  }

  componentDidMount() {
    const { filters } = this.state;
    this.formRef.current.fill(filters);
    window.addEventListener('popstate', this.syncAppFromQueryString, false);
    this.loadApplications();
  }

  componentWillUnmount() {
    this.requestApplicationsTokenSource.cancel('Page is being unmounted.');
    window.removeEventListener('popstate', this.syncAppFromQueryString, false);
  }

  getPositionsList() {
    const { data } = this.state;
    const allPositions = data.map(item => item.position_applied);
    const allPositionsSet = new Set(allPositions);
    const positions = Array.from(allPositionsSet.values());
    positions.sort();
    return positions.map(p => ({ label: p, value: p }));
  }

  getFilteredData() {
    const {
      data,
      filters: { status, position, name },
    } = this.state;

    let filteredData = [...data];

    if (status) {
      filteredData = filteredData.filter(item => item.status === status);
    }

    if (position) {
      filteredData = filteredData.filter(item => item.position_applied === position);
    }

    const trimmedName = name ? name.trim() : null;
    if (trimmedName) {
      const regex = new RegExp(trimmedName, 'i');
      filteredData = filteredData.filter(item => regex.test(item.name));
    }

    return filteredData;
  }

  syncAppFromQueryString = () => {
    const { name, position, status, sortBy, sortType } = QueryString.all();
    const filters = { name: name || '', position: position || '', status: status || '' };
    this.setState({ filters, sortBy, sortType });
    this.formRef.current.fill(filters);
  };

  handleOnChangeForm = filters => {
    this.setState({ filters }, () => {
      this.updateUrl();
    });
  };

  handleOnSort = (sortBy, sortType) => {
    this.setState({ sortBy, sortType }, () => {
      this.updateUrl();
    });
  };

  loadApplications() {
    request
      .get('candidates', { cancelToken: this.requestApplicationsTokenSource.token })
      .then(response => {
        this.setState({ data: response.data.data, isLoading: false });
      })
      .catch(() => {
        this.setState({
          errorMessage: 'An error occurred loading applications list. Please, try again later',
          isLoading: false,
        });
      });
  }

  updateUrl() {
    const { filters, sortBy, sortType } = this.state;
    const params = {};

    Object.keys(filters).forEach(key => {
      const value = String(filters[key]).trim();
      if (value) {
        params[key] = value;
      }
    });

    if (sortBy) {
      params.sortBy = sortBy;
    }

    if (sortType) {
      params.sortType = sortType;
    }

    QueryString.set(params);
  }

  render() {
    const filteredData = this.getFilteredData();
    const positionsList = this.getPositionsList();
    const { sortBy, sortType, isLoading, errorMessage } = this.state;
    return (
      <Page>
        <Container>
          <Title text="Applications" icon={<UserIcon />} />
          {errorMessage && (
            <React.Suspense fallback={null}>
              <Alert title="Error loading data" text={errorMessage} />
            </React.Suspense>
          )}
          <Datagrid
            title="Candidates"
            toolbar={
              <Form onChange={this.handleOnChangeForm} ref={this.formRef}>
                <Container direction="row">
                  <Input label="Name" name="name" />
                  <Select
                    label="Position"
                    name="position"
                    emptyOption="All"
                    items={positionsList}
                  />
                  <RadioGroup
                    label="Status"
                    name="status"
                    items={[
                      { label: 'All', value: '' },
                      { label: 'Approved', value: 'approved' },
                      { label: 'Rejected', value: 'rejected' },
                      { label: 'Waiting', value: 'waiting' },
                    ]}
                  />
                </Container>
              </Form>
            }
            isLoading={isLoading}
            data={filteredData}
            onSort={this.handleOnSort}
            sortBy={sortBy}
            sortType={sortType}
            rowColumns={[
              'name',
              'email',
              'birth_date',
              'year_of_experience',
              'position_applied',
              'application_date',
              'status',
            ]}
            headers={[
              'Name',
              'Email',
              'Age',
              { label: 'Years of Experience', sortable: true },
              { label: 'Position applied', sortable: true },
              { label: 'Applied', sortable: true },
              'Status',
            ]}
            formatter={{
              birth_date: value => ageByDate(value),
              application_date: value => formatDate(value),
            }}
          />
        </Container>
      </Page>
    );
  }
}

export default Applications;
